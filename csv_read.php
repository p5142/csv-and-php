<?php
require "connection.php";
$file = fopen("../people.csv","r");
if($file !== FALSE) {
    while(!feof($file)) {
        $data = fgetcsv($file);   
        if($data[0] == "id"){
            continue;
        }
        $id = $data[0];
        $firstname = $data[1];
        $lastname = $data[2];
    /////////////////////////////////////////////////////////////////////VALID EMAIL////////////////////////////////////////////////////////////////
        $email = filter_var($data[3], FILTER_VALIDATE_EMAIL) ? $email = strtolower($data[3]) : NULL;
    ////////////////////////////////////////////////////////////////////VALID JOB///////////////////////////////////////////////////////////////////
        $job = ($data[4] == "politicien") ? ($data[4] = "menteur") : $data[4];
        $birthdate = $data[5];
        // $country = $data[6];
    //////////////////////////////////////////////////////////////////////VALID PHONE NUMBER////////////////////////////////////////////////////////////////
        $oldstr = $data[7];
        $str_to_insert = "0";
        $pos= 0;
        $newstr = substr_replace($oldstr, $str_to_insert, $pos, 0); 
        $phone_french = '/^(0)[1-9](\d{2}){4}$/';
        $phone =  preg_match($phone_french, $newstr) === 1 ? $newstr : NULL;
    ////////////////////////////////////////////////////////////////////////API FOR COUNTRY CODE////////////////////////////////////////////////////////////
        $response = file_get_contents('https://restcountries.com/v3.1/all?fields=region,cca2');
        $response = json_decode($response);
        // $response->[cca2]->[region];
        foreach($response as $shortres){
            $api_region = $shortres->region;
            $api_code = $shortres->cca2;  
            $country = $data[6] == $api_code ? $api_region : NULL ;
        }
          
        // cca2 

        // try {    
        // $sql2 = $conn->prepare("INSERT INTO csv_php.people (id, firstname, lastname, email, profession, birthdate, country, phone) VALUES (:new_id, :new_firstname, :new_lastname, :new_email, :new_profession, :new_birthdate, :new_country, :new_phone)");
        // $sql2->bindParam('new_id', $id, PDO::PARAM_INT);
        // $sql2->bindParam('new_firstname', $firstname, PDO::PARAM_STR);
        // $sql2->bindParam('new_lastname', $lastname, PDO::PARAM_STR);
        // $sql2->bindParam('new_email', $email, PDO::PARAM_STR);
        // $sql2->bindParam('new_profession', $job, PDO::PARAM_STR);
        // $sql2->bindParam('new_birthdate', $birthdate, PDO::PARAM_STR);
        // $sql2->bindParam('new_country', $country, PDO::PARAM_STR);
        // $sql2->bindParam('new_phone', $phone, PDO::PARAM_INT);
        // $sql2->execute();
        // }catch(PDOException $e){
        // echo "Error : ".$e->getMessage();
        // }
    }
}

fclose($file);
?>